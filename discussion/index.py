print("Hello World!")

# [SECTION] Declaring Variables
print("[SECTION] Declaring Variables")

age = 29
middle_initial = "D"


# Python allows assigning of values to multiple variables within a single line
name1, name2, name3, name4 = "John", "Paul", "Goerge", "Ringo"

# [SECTION] Data Types
print("[SECTION] Data Types")

# 1. Strings - for alphanumeric characters and symbols
full_name = "John Doe"
secret_code = "Pa$$word"

# 2. Numbers - for integers, decimals, and complex numbers
number_of_days = 365 # This is an integer
pi_approximation = 3.1416 # This is a decimal
complex_num = 1+5j # This is a complex numbers

# 3. Boolean - true or false values
isLearning = True
isDifficult = False

# [SECTION] Using Variables
print("[SECTION] Using Variables")

print ("My name is " + full_name)
print ("My age is " + str(age)) 
# Typecasting parses data to convert it to a different data type

print(int(3.5)) # Converts number value into an integer
print(float(5)) # Converts number value into a float/decimal

# F-Strings or “formatted string literals” (same as Template Literals in JS)
print(f"Hi! My name is {full_name} and my age is {age}")
# this auto-parse (all data types can coexist)

# [SECTION] Operations
print("[SECTION] Operations")

# Arithmetic Operators - for mathematical operations
print (1+10) # Addition
print (15-9) # Subtraction
print (18*9) # Multiplication
print (21/7) # Division
print (18%4) # Modulo (get remainder)
print (2 ** 6) # Exponent

# Assignment Operators
num1 = 3
num1 +=4
print (num1)

# Comparison Operators
print (1 == 1)

# Logical Operators
print (True and False)
print (not False)
print (False or True)