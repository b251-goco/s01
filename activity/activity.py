# [ACTIVITY] The goal of the activity is to be able to 'create variables' and 'perform simple operations' using Python syntax

# 1. Create an 'activity' folder and an 'activity.py' file inside of it.
# 2. 'Create 5 variables' and 'output them in the command prompt' in the following format
	# a. "I am <name (string)>, and I am <age (integer)> years old. I work as a <occupation (string)> and my rating for <movie (string)> is <rating (decimal)>%."

name = "Jygs"
age = 24
occupation = "SHS Teacher"
movie = "The Menu"
rating = 96.00

print(f"I am {name}, and I am {age} years old. I work as a {occupation} and my rating for \"{movie}\" is {rating}%")

# 3. 'Create 3 variables', num1, num2, and num3
	# a. get the 'product of num1 and num2'
	# b. check if 'num1 is less then num3'
	# c. 'Add the value' of 'num3 to num 2'

num1, num2, num3 = 11, 15, 24

a = num1*num2
b = num1 <= num3
c = num2 + num3
print(a)
print(b)
print(c)

# 4. 'Create Git Repository' named s01 inside your 'provided GitLab Subfolder'
	# in GitLab, create new project under b251 group
	# make it public, no checks
	# copy 'SSH code' from clone
	# s01, git init, git remote add origin <paste>
# 5. 'Initialize a git repository', 'stage the files' in preparation for a commit, 'create a commit' with the message 'Add Activity Code' and 'push the changes' to the remote repository.
	# git add .
	# git commit -m "<message here>"
	# git push origin master
	# git log --oneline




